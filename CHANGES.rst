Changelog
=========

1.0
---

* Released on 22th January 2019.
* Discover Joomla INI files.

0.3
---

* Released on 6th December 2018.
* Code restructuring.
* Better handling of multiple language codes in path.
* Extended test cases.

0.2
---

* Released on 30th November 2018.
* Added detection for monolingual Gettext, XLIFF and web extension.
* Detect new base for Gettext and Qt TS.
* Detect encoding of properties files.
* Automatically import Transifex configuration.

0.1
---

* Released on 19th October 2018.
* Initial release.
